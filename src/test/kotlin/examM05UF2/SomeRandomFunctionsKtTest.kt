package examM05UF2

import org.junit.jupiter.api.Test

internal class SomeRandomFunctionsKtTest {
// He cambiado el nombre de algunas funciones y he cambiado casi todos los val y var
    @Test //nombre incorrecto de funcion
    fun numbersBetweenList(){ //Se introduce dos valores de tipo Int y
                            // los numeros del primer valor al segundo son añadidos a una lista (de Int)
        val firstvalue = 3
        val secondvalue = 5
        val numero = mutableListOf<Int>()
        for (i in firstvalue..secondvalue) {  //El test falla si el el primer valor (m : Int) es menor que el segundo valor (n : Int)
            numero.add(i)
        }
        assert(numero.size == 3) //Verifica la cantidad de valores de tipo Int dentro de la lista

    //retorna la llista de Int (numero)
    }

    @Test //nombre correcto de funcion
    fun minValue(){ // Se introduce una lista de tipo Int y deberia dar el valor más pequeño de la lista
        val numberList = List<Int>(3){2; 3; 1 }
        var minValue = 1
        for (i in numberList.indices) {
            if (i < minValue) {
                minValue = i
            } else if (i == minValue) {
                minValue = i
            }
        }
        assert(minValue == 0) //Deberia dar el valor minimo de los valores de la lista pero da un valor incorrecto
        //retorna b
    }

    @Test //nombre incorrecto de funcion
    fun qualifications() { //Introduces un valor Int y hace return de un string
        val qualificationNumber = 2
        val qualification : String
        when (qualificationNumber) {
            1, 2, 3, 4 -> qualification = "Insuficient" //return
            0 -> qualification = "No presentat" //return
            10 -> qualification = "Excelent + MH" //return
            5, 6 -> qualification ="Suficient" //return
            7 ->  qualification = "Notable" //return
            else -> qualification = "No valid" //return
        }
        assert(qualification == "Insuficient") //verifica que el string del return es el que está vinculado al numero del when
    }

    @Test //nombre incorrecto de funcion
    fun selectSpecificLetters() { //introduces una palabra (String) y una letra (Char) y guarda las letras en una lista (luego hace return de la lista)
        val word = "patata"
        val letter : Char = 'a'
        val letterCoincidences = mutableListOf<Int>()
        var coincidencies = 0 //Esta varible no se usa
        for (i in 0 until word.lastIndex) {
            if (word[i] == letter) {
                letterCoincidences.add(i)
            }
        }
        assert(word[1]==letter) // verificar que la letra se encuentra en la palabra
        assert(word[3]==letter)
        assert(word[5]==letter)
        //return letterCoincidences
    }

    @Test //nombre correcto de funcion
    fun firstOccurrencePosition() { //introduces una palabra (String) y una letra (Char) y hace return de la posición
        val pos: MutableList<Int> = mutableListOf() //las posiciones deben guardarse aquí (no se está usando este val)
        val word = "hola"
        val letter = 'o'

        for (i in 0 until word.length) { //el until no llega al ultimo valor, en caso de que la ultima letra sea la primera letra buscada no dará el valor correcto
            if (word[i] == letter) {
                //return i
            }
        }
        assert(word[1]== letter)  //verifica que la letra de la posicion es la letra indicada
        //return -1
    }

    @Test //nombre de funcion incorrecto
    fun truncateDoubles() { //Introduces un numero decimal (Double) y te lo pasa a numero entero (Int)
        val grade : Double = 7.7

        assert(grade.toInt() == 7) //verifica que trunca el numero eliminando todos los decimales (pasa de Double a Int)
    //return grade.toInt()
    }
}