package examM05UF2

//input m=3, n=7 output 3 4 5 6 7
fun needFunctionName(m: Int, n: Int): List<Int> {
    val numero = mutableListOf<Int>()
    for (i in m..n) {
        numero.add(i)
    }
    return numero
}

fun minValue(a: List<Int>): Int {
    var b = 0
    for (i in a.indices) {
        if (i < b) {
            b = i
        } else if (i == b) {
            b = i
        }
    }
    return b
}


// S'esepra el seguent comportament
//  0: No presentat
//  1-4: Insuficient
//  5-6: Suficient
//  7-8: Notable
//  9: Excel·lent
//  10: Excel·lent + MH
fun select(i: Int): String {
    //TODO: Cahotic order. Make it easy, only one return.
    when (i) {
        1, 2, 3, 4 -> return "Insuficient"
        0 -> return "No presentat"
        10 -> return "Excelent + MH"
        5, 6 -> return "Suficient"
        7 -> return "Notable"
        else -> return "No valid"
    }
}

// De tota la paraula
fun charPositionsInString(abc: String, b: Char): List<Int> {
    val s = mutableListOf<Int>()
    var coincidencies = 0
    for (i in 0 until abc.lastIndex) {
        if (abc[i] == b) {
            s.add(i)
        }
    }
    return s
}


//abc="marieta" b='a' -> 1
//abc="marieta" b='b' -> -1
fun firstOccurrencePosition(abc: String, b: Char): Int {
    val pos: MutableList<Int> = mutableListOf()
    for (i in 0 until abc.length) {
        if (abc[i] == b) {
            return i
        }
    }
    return -1
}

/*
La funció ha de retornar la nota arredonida. 8.7 -> 9, 7.5 -> 8
Excepció, Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4
 */
fun note(grade: Double): Int {
    return grade.toInt()
}